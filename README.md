A silly test of Traits with members gated by features *traitcfg*,
and libraries using trait objects seeing differently gated features *usebar*, *usebaz*,
are conjoined in some enclosing program *barbaz*

there are 2 variations of this, an object safe one,
and a non object safe one, this works (suprisingly to me) how one might want them to
when mixing into a compilation unit:

bar cannot call baz,
baz cannot call bar,
but bar/baz oblivious to any object safety violating methods,
fail in their attempt to create a trait object.

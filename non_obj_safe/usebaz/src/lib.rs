extern crate traitcfg;
use traitcfg::Foo;

pub fn do_something(x:&Foo) {
   println!("{} {}", x.okay(), x.has_baz());
}

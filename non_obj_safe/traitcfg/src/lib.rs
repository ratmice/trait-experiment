
pub trait Foo {
    fn okay(&self) -> bool;
    #[cfg(feature = "bar")]
    fn has_bar(&self) -> bool;
    #[cfg(feature = "baz")]
    fn has_baz(&self) -> bool;
    #[cfg(feature = "nonobjsafe")]
    fn not_object_safe<T>(&self, _x: T);
}

impl Foo for () {
    fn okay (&self) -> bool {
       true
    }
    #[cfg(feature="bar")]
    fn has_bar(&self) -> bool {
      true
    }
  
    #[cfg(feature="baz")]
    fn has_baz(&self) -> bool {
      true
    }

    #[cfg(feature="nonobjsafe")]
    fn not_object_safe<T>(&self, _x: T)
    {

    }

}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

extern crate traitcfg;
use traitcfg::Foo;

pub fn do_something<T: Foo>(x:T) {
   println!("{} {}", x.okay(), x.has_bar());
}


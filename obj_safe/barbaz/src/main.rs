extern crate usebar;
extern crate usebaz;



fn main() {
    let foo = {};
    usebar::do_something(foo);
    usebaz::do_something(foo);
}

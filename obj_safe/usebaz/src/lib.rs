extern crate traitcfg;
use traitcfg::Foo;

pub fn do_something<T: Foo>(x:T) {
   println!("{} {}", x.okay(), x.has_baz());
}

#[cfg(not(feature = "nonobjsafe"))]
pub fn do_something_objsafe(x:&Foo) {
   println!("{} {}", x.okay(), x.has_baz());
}
